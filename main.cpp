//#include "mainwindow.h"
#include <QApplication>
#include <QLabel>

int main(int argc, char *argv[])
{
    static QApplication a(argc, argv);
    //MainWindow w;
    static QLabel label("Hello Qt Amira");
    QFont font = label.font();
    font.setPointSize(72);
    font.setBold(true);
    label.setFont(font);
    label.show();
   // w.show();
    return a.exec();
}
